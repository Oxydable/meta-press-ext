= Technical Documentation
:lang: en
:experimental: yes
:toc: auto
:toc-title: Index:

//== New source Form

//== Unit test

== Test integrity
The test integrity is a set of tests which verifie if a source 
is correctly written. All of this test are describe in a file: 
link:json/test_integrity.json[test_integrity].

=== Test integrity file format
At the top of the document we have the `base` attribute, this list
describe the necessary fields in a source. After that we found the `rss`
entries. RSS attribute contain the different possible definition for the rss, atom files.

WARNING: The `rss` attributes describe the way of rss treatment in *ALL* Metapress

The `format_order` is the list of all possible attribute in a source (_xpath, _attr, _re, _com version is implicite). The order of the list are used for sort the different attributes in source.

The next part of the file is the test description of the different attributes. 

```json
"favicon_url": {
    "conflicts": [], <1>
    "dependencies": [], <2>
    "test_re_url": true, <3>
    "test": ["url"], <4>
    "url_code": 200, <5>
    "test_level": "warn", <6>
    "class_list": ["base"] <7>
}
```

<1> The incompatible list attributes.
<2> The list of needed attributes.
<3> This attribute create or *replace* the re test for test the url validity of the attribute
<4> The list of test to execute.
<5> Needed subattribut for url test.
<6> The error level of tests "error" is the default. 
<7> The show group for the new source form.

List of the possible test:

 * `re` a regex test, the regex is given with `test_re` attributes (string)
 * `key` test if the attribute value is present in a given object by `test_object` or in a file `test_object_file`
 * `inSource` check if attribute is present in the sources boject given (see doc test_integrity, test_attribute)
 * `values` test if attribute value is the same given by `test_values`
 * `oneofvalues` test if attribute value is present in the list given by  `test_oneofvalues` or a file `test_oneofvalues_file`
 * `url` test if url is correct and test if the server url respond the given code in `url_code` (list of integer).
